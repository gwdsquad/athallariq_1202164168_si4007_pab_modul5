package com.athallariqr.modul5;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends AppCompatActivity {

    RelativeLayout parent;
    Button saveButton;
    Switch nightModeSwitch, fontSizeSwitch;
    Boolean isNightMode = false;
    Boolean isBigFontSize = false;

    TextView settingText, nightmodeText, fontsizeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        parent = findViewById(R.id.parent);
        saveButton = findViewById(R.id.saveButton);
        nightModeSwitch = findViewById(R.id.nightModeSwitch);
        fontSizeSwitch = findViewById(R.id.fontSizeSwitch);

        fontsizeText = findViewById(R.id.fontText);
        settingText = findViewById(R.id.settingText);
        nightmodeText = findViewById(R.id.nightmodeText);

        init();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("isNightMode", nightModeSwitch.isChecked());
                editor.putBoolean("isBigFontSize", fontSizeSwitch.isChecked());
                editor.apply();

                Toast.makeText(SettingActivity.this, "Setting saved successfully", Toast.LENGTH_SHORT).show();

                finish();
            }
        });
    }

    private void init(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isNightMode = pref.getBoolean("isNightMode", isNightMode);
        isBigFontSize = pref.getBoolean("isBigFontSize", isBigFontSize);

        nightModeSwitch.setChecked(isNightMode);
        fontSizeSwitch.setChecked(isBigFontSize);

        if(isNightMode){

            fontsizeText.setTextColor(Color.WHITE);
            settingText.setTextColor(Color.WHITE);
            nightmodeText.setTextColor(Color.WHITE);
            parent.setBackgroundColor(Color.BLACK);
        } else {

            fontsizeText.setTextColor(Color.BLACK);
            settingText.setTextColor(Color.BLACK);
            nightmodeText.setTextColor(Color.BLACK);
            parent.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
