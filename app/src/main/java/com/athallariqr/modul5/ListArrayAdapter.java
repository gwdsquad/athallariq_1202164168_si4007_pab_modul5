package com.athallariqr.modul5;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ListArrayAdapter extends ArrayAdapter<Article> {
    private final Context context;
    private final List<Article> values;

    public ListArrayAdapter(Context context, List<Article> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder") View rowView = inflater.inflate(R.layout.list_row, parent, false);
        TextView titleText, authorText, articleText;
        titleText = rowView.findViewById(R.id.titleArticleText);
        authorText = rowView.findViewById(R.id.authorArticleText);
        articleText = rowView.findViewById(R.id.descArticleText);

        titleText.setText(values.get(position).getTitle());
        authorText.setText(values.get(position).getAuthor());
        articleText.setText(values.get(position).getDescription());

        return rowView;
    }
}