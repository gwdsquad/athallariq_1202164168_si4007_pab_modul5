package com.athallariqr.modul5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
 
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "articles_db";
 
 
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
 
        // create notes table
        db.execSQL(Article.CREATE_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Article.TABLE_NAME);
 
        // Create tables again
        onCreate(db);
    }

    public void insertArticle(String title, String description, String author) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Article.COLUMN_TITLE, title);
        values.put(Article.COLUMN_DESCRIPTION, description);
        values.put(Article.COLUMN_AUTHOR, author);

        // insert row
        db.insert(Article.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
    }

    public List<Article> getAllArticles() {
        List<Article> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Article.TABLE_NAME + " ORDER BY " +
                Article.COLUMN_CREATED_AT + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Article note = new Article();
                note.setId(cursor.getInt(cursor.getColumnIndex(Article.COLUMN_ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(Article.COLUMN_TITLE)));
                note.setDescription(cursor.getString(cursor.getColumnIndex(Article.COLUMN_DESCRIPTION)));
                note.setAuthor(cursor.getString(cursor.getColumnIndex(Article.COLUMN_AUTHOR)));
                note.setCreated_at(cursor.getString(cursor.getColumnIndex(Article.COLUMN_CREATED_AT)));

                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        cursor.close();
        db.close();

        // return notes list
        return notes;
    }
}