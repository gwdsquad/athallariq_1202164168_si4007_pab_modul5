package com.athallariqr.modul5;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class CreateActivity extends AppCompatActivity {

    RelativeLayout parent;
    Button postButton;
    EditText titleET, authorET, articleET;
    Boolean isNightMode = false;

    TextView createText, titlesubText, articlesubText, authorsubText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        parent = findViewById(R.id.parent);
        postButton = findViewById(R.id.postButton);
        titleET = findViewById(R.id.input_title);
        authorET = findViewById(R.id.input_author);
        articleET = findViewById(R.id.input_article);

        createText = findViewById(R.id.createText);
        titlesubText = findViewById(R.id.titlesubText);
        articlesubText = findViewById(R.id.articlesubText);
        authorsubText = findViewById(R.id.authorsubText);

        init();

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(titleET.getText().toString().equals("")){
                    Toast.makeText(CreateActivity.this, "Input title first", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(authorET.getText().toString().equals("")){
                    Toast.makeText(CreateActivity.this, "Input author first", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(articleET.getText().toString().equals("")){
                    Toast.makeText(CreateActivity.this, "Input article first", Toast.LENGTH_SHORT).show();
                    return;
                }

                DatabaseHelper helper = new DatabaseHelper(CreateActivity.this);
                helper.insertArticle(titleET.getText().toString(), articleET.getText().toString(), authorET.getText().toString());
                Toast.makeText(CreateActivity.this, "Article created successfully", Toast.LENGTH_SHORT).show();

                titleET.setText("");
                authorET.setText("");
                articleET.setText("");
            }
        });
    }

    private void init(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isNightMode = pref.getBoolean("isNightMode", isNightMode);

        if(isNightMode){

            createText.setTextColor(Color.WHITE);
            titlesubText.setTextColor(Color.WHITE);
            articlesubText.setTextColor(Color.WHITE);
            authorsubText.setTextColor(Color.WHITE);
            titleET.setTextColor(Color.WHITE);
            authorET.setTextColor(Color.WHITE);
            articleET.setTextColor(Color.WHITE);
            titlesubText.setBackgroundColor(Color.BLACK);
            articlesubText.setBackgroundColor(Color.BLACK);
            authorsubText.setBackgroundColor(Color.BLACK);
            parent.setBackgroundColor(Color.BLACK);
        } else {

            createText.setTextColor(Color.BLACK);
            titlesubText.setTextColor(Color.BLACK);
            articlesubText.setTextColor(Color.BLACK);
            authorsubText.setTextColor(Color.BLACK);
            titleET.setTextColor(Color.BLACK);
            authorET.setTextColor(Color.BLACK);
            articleET.setTextColor(Color.BLACK);
            titlesubText.setBackgroundColor(Color.WHITE);
            articlesubText.setBackgroundColor(Color.WHITE);
            authorsubText.setBackgroundColor(Color.WHITE);
            parent.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
