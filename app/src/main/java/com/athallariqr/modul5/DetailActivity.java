package com.athallariqr.modul5;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity extends AppCompatActivity {

    TextView titleText, authorText, articleText;
    String title, author, article, date;
    RelativeLayout parent;

    Boolean isNightMode = false, isBigFontSize = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        parent = findViewById(R.id.parent);
        titleText = findViewById(R.id.titleBigText);
        authorText = findViewById(R.id.authorBigText);
        articleText = findViewById(R.id.articleBigText);

        init();

        title = getIntent().getStringExtra("EXTRA_TITLE");
        article = getIntent().getStringExtra("EXTRA_DESC");
        author = getIntent().getStringExtra("EXTRA_AUTHOR");
        date = getIntent().getStringExtra("EXTRA_DATE");

        titleText.setText(title);
        articleText.setText(article);
        //Mon 1 Apr '19
        //2019-04-06
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = sdf.parse(date);
            authorText.setText(author + ", " + parseDay(d.getDay()) + " " + d.getDate() + " " + parseMonth(d.getMonth()) + " '" + String.valueOf(d.getYear()).substring(1,3));
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }
    }

    private String parseDay(int day){
        String result = null;
        switch(day){
            case 0: result = "Mon";
                break;
            case 1: result = "Tue";
                break;
            case 2: result = "Wed";
                break;
            case 3: result = "Thu";
                break;
            case 4: result = "Fri";
                break;
            case 5: result = "Sat";
                break;
            case 6: result = "Sun";
                break;
        }
        return result;
    }

    private String parseMonth(int month){
        String result = null;
        switch(month){
            case 0: result = "Jan";
                break;
            case 1: result = "Feb";
                break;
            case 2: result = "Mar";
                break;
            case 3: result = "Apr";
                break;
            case 4: result = "May";
                break;
            case 5: result = "Jun";
                break;
            case 6: result = "Jul";
                break;
            case 7: result = "Aug";
                break;
            case 8: result = "Sep";
                break;
            case 9: result = "Oct";
                break;
            case 10: result = "Nov";
                break;
            case 11: result = "Dec";
                break;
        }
        return result;
    }

    private void init(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isNightMode = pref.getBoolean("isNightMode", isNightMode);
        isBigFontSize = pref.getBoolean("isBigFontSize", isBigFontSize);

        if(isNightMode){
            titleText.setTextColor(Color.WHITE);
            authorText.setTextColor(Color.WHITE);
            articleText.setTextColor(Color.WHITE);
            parent.setBackgroundColor(Color.BLACK);
        } else {
            titleText.setTextColor(Color.BLACK);
            authorText.setTextColor(Color.BLACK);
            articleText.setTextColor(Color.BLACK);
            parent.setBackgroundColor(Color.WHITE);
        }

        if(isBigFontSize){
            articleText.setTextSize(50);
        } else {
            articleText.setTextSize(20);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
