package com.athallariqr.modul5;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

public class ListActivity extends AppCompatActivity {

    ListView listArticleView;
    RelativeLayout parent;
    Boolean isNightMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listArticleView = findViewById(R.id.listView);
        parent = findViewById(R.id.parent);

        init();

        DatabaseHelper helper = new DatabaseHelper(ListActivity.this);
        final List<Article> articles;
        articles = helper.getAllArticles();

        ListArrayAdapter adapter = new ListArrayAdapter(ListActivity.this, articles);
        listArticleView.setAdapter(adapter);

        listArticleView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(ListActivity.this, DetailActivity.class);
                intent.putExtra("EXTRA_TITLE", articles.get(position).getTitle());
                intent.putExtra("EXTRA_DESC", articles.get(position).getDescription());
                intent.putExtra("EXTRA_AUTHOR", articles.get(position).getAuthor());
                intent.putExtra("EXTRA_DATE", articles.get(position).getCreated_at());
                startActivity(intent);
            }
        });
    }

    private void init(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isNightMode = pref.getBoolean("isNightMode", isNightMode);

        if(isNightMode){
            parent.setBackgroundColor(Color.BLACK);
        } else {
            parent.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
