package com.athallariqr.modul5;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RelativeLayout parent;
    TextView titleText;
    Boolean isNightMode = false;
    Button listArticleButton, createArticleButton, settingButton;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = findViewById(R.id.parent);
        titleText = findViewById(R.id.titleText);
        listArticleButton = findViewById(R.id.listArticleButton);
        createArticleButton = findViewById(R.id.createArticleButton);
        settingButton = findViewById(R.id.settingButton);

        init();

        createArticleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, CreateActivity.class));
            }
        });

        listArticleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        });

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, SettingActivity.class));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void init(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        isNightMode = pref.getBoolean("isNightMode", isNightMode);

        if(isNightMode){
            parent.setBackgroundColor(Color.BLACK);
            titleText.setText("Good Night");
            titleText.setTextColor(Color.WHITE);
        } else {
            parent.setBackgroundColor(Color.WHITE);
            titleText.setText("Good Morning");
            titleText.setTextColor(Color.BLACK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
